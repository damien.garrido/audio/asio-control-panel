﻿using System;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.Win32;

namespace AsioControlPanel
{
    public partial class FormAsioControlPanel : Form
    {
        private RegistryKey registryKey = null;

        private Font NormalFont;

        private Font BoldFont;

        public FormAsioControlPanel()
        {
            registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            // Load driver names
            string[] driverNames = AsioDriver.GetAsioDriverNames();

            NormalFont = new Font("Segoe UI", 9);
            BoldFont = new Font("Segoe UI", 9, FontStyle.Bold);

            InitializeComponent();

            // Update checkBoxStartsWithWindows
            if (registryKey.GetValue(Application.ProductName) == null)
            {
                checkBoxStartsWithWindows.Checked = false;
            }
            else
            {
                checkBoxStartsWithWindows.Checked = true;
            }

            // Update Show/Hide menu item fonts
            showToolStripMenuItem.Font = BoldFont;
            hideToolStripMenuItem.Font = NormalFont;

            // Populate combo box
            comboBoxAsioDriverNames.Items.AddRange(items: driverNames);

            HideIt();
        }

        private void checkBoxStartsWithWindows_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxStartsWithWindows.Checked)
            {
                registryKey.SetValue(Application.ProductName, '"' + Application.ExecutablePath + '"');
            }
            else
            {
                registryKey.DeleteValue(Application.ProductName, false);
            }
        }

        private void ComboBoxAsioDriverNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!(comboBoxAsioDriverNames.SelectedItem is null))
            {
                buttonShowControlPanel.Enabled = true;
            }
        }

        private void ButtonShowControlPanel_Click(object sender, EventArgs args)
        {
            try
            {
                using (var asio = new AsioOut(comboBoxAsioDriverNames.SelectedItem.ToString()))
                {
                    asio.ShowControlPanel();
                    asio.Dispose();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", buttons: MessageBoxButtons.OK, icon: MessageBoxIcon.Error);
            }
        }

        private bool allowVisible = false;
        
        protected override void SetVisibleCore(bool value)
        {
            if (!allowVisible)
            {
                value = false;
                if (!this.IsHandleCreated) CreateHandle();
            }
            base.SetVisibleCore(value);
        }
        

        private bool shown = true;

        private void ShowIt()
        {
            allowVisible = true;
            Show();
            WindowState = FormWindowState.Normal;
            showToolStripMenuItem.Font = NormalFont;
            hideToolStripMenuItem.Font = BoldFont;
            shown = true;
        }

        private void HideIt()
        {
            WindowState = FormWindowState.Minimized;
            Hide();
            showToolStripMenuItem.Font = BoldFont;
            hideToolStripMenuItem.Font = NormalFont;
            shown = false;
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (shown)
                HideIt();
            else
                ShowIt();
        }

        private void ShowToolStripMenuItem_Click(object sender, EventArgs e) => ShowIt();

        private void HideToolStripMenuItem_Click(object sender, EventArgs e) => HideIt();

        private bool realClosing = false;

        private void QuitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            realClosing = true;
            Close();
        }

        private static bool CancelQuit() => MessageBox.Show("Are you sure ?", "Quit " + Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No;

        private void FormAsioControlPanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (realClosing)
            {
                realClosing = false;
                e.Cancel = CancelQuit();
            }
            else
            {
                e.Cancel = true;
                HideIt();
            }
        }
    }
}
