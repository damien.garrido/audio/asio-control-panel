﻿namespace AsioControlPanel
{
    /// <summary>
    /// ASIO Error Codes
    /// </summary>
    public enum AsioError
    {
        /// <summary>
        /// This value will be returned whenever the call succeeded
        /// </summary>
        ASE_OK = 0,
        /// <summary>
        /// unique success return value for ASIOFuture calls
        /// </summary>
        ASE_SUCCESS = 0x3f4847a0,
    }
}
