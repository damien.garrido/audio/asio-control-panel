﻿using System;

namespace AsioControlPanel
{
    // -------------------------------------------------------------------------------
    // Structures used by the AsioDriver and ASIODriverExt
    // -------------------------------------------------------------------------------

    // -------------------------------------------------------------------------------
    // Error and Exceptions
    // -------------------------------------------------------------------------------

    /// <summary>
    /// ASIO common Exception.
    /// </summary>
    internal class AsioException : Exception
    {
        private AsioError error;

        public AsioException()
        {
        }

        public AsioException(string message)
            : base(message)
        {
        }

        public AsioException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public AsioError Error
        {
            get { return error; }
            set
            {
                error = value;
                Data["ASIOError"] = error;
            }
        }

        /// <summary>
        /// Gets the name of the error.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <returns>the name of the error</returns>
        public static String getErrorName(AsioError error)
        {
            return Enum.GetName(typeof(AsioError), error);
        }
    }
}
