﻿namespace AsioControlPanel
{
    partial class FormAsioControlPanel
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAsioControlPanel));
            this.comboBoxAsioDriverNames = new System.Windows.Forms.ComboBox();
            this.labelAsioDriverNames = new System.Windows.Forms.Label();
            this.buttonShowControlPanel = new System.Windows.Forms.Button();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkBoxStartsWithWindows = new System.Windows.Forms.CheckBox();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxAsioDriverNames
            // 
            this.comboBoxAsioDriverNames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAsioDriverNames.FormattingEnabled = true;
            this.comboBoxAsioDriverNames.Location = new System.Drawing.Point(15, 25);
            this.comboBoxAsioDriverNames.Name = "comboBoxAsioDriverNames";
            this.comboBoxAsioDriverNames.Size = new System.Drawing.Size(281, 21);
            this.comboBoxAsioDriverNames.TabIndex = 0;
            this.comboBoxAsioDriverNames.SelectedIndexChanged += new System.EventHandler(this.ComboBoxAsioDriverNames_SelectedIndexChanged);
            // 
            // labelAsioDriverNames
            // 
            this.labelAsioDriverNames.AutoSize = true;
            this.labelAsioDriverNames.Location = new System.Drawing.Point(12, 9);
            this.labelAsioDriverNames.Name = "labelAsioDriverNames";
            this.labelAsioDriverNames.Size = new System.Drawing.Size(147, 13);
            this.labelAsioDriverNames.TabIndex = 1;
            this.labelAsioDriverNames.Text = "Please select an ASIO Driver:";
            // 
            // buttonShowControlPanel
            // 
            this.buttonShowControlPanel.Enabled = false;
            this.buttonShowControlPanel.Location = new System.Drawing.Point(102, 57);
            this.buttonShowControlPanel.Name = "buttonShowControlPanel";
            this.buttonShowControlPanel.Size = new System.Drawing.Size(112, 23);
            this.buttonShowControlPanel.TabIndex = 2;
            this.buttonShowControlPanel.Text = "Show Control Panel";
            this.buttonShowControlPanel.UseVisualStyleBackColor = true;
            this.buttonShowControlPanel.Click += new System.EventHandler(this.ButtonShowControlPanel_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.contextMenuStrip;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "ASIO Control Panel";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem,
            this.hideToolStripMenuItem,
            this.toolStripSeparator1,
            this.quitToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(104, 76);
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem.Text = "Show";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.ShowToolStripMenuItem_Click);
            // 
            // hideToolStripMenuItem
            // 
            this.hideToolStripMenuItem.Name = "hideToolStripMenuItem";
            this.hideToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.hideToolStripMenuItem.Text = "Hide";
            this.hideToolStripMenuItem.Click += new System.EventHandler(this.HideToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(100, 6);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.QuitToolStripMenuItem_Click);
            // 
            // checkBoxStartsWithWindows
            // 
            this.checkBoxStartsWithWindows.AutoSize = true;
            this.checkBoxStartsWithWindows.Location = new System.Drawing.Point(15, 96);
            this.checkBoxStartsWithWindows.Name = "checkBoxStartsWithWindows";
            this.checkBoxStartsWithWindows.Size = new System.Drawing.Size(122, 17);
            this.checkBoxStartsWithWindows.TabIndex = 3;
            this.checkBoxStartsWithWindows.Text = "Starts with Windows";
            this.checkBoxStartsWithWindows.UseVisualStyleBackColor = true;
            this.checkBoxStartsWithWindows.CheckedChanged += new System.EventHandler(this.checkBoxStartsWithWindows_CheckedChanged);
            // 
            // FormAsioControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 125);
            this.Controls.Add(this.checkBoxStartsWithWindows);
            this.Controls.Add(this.buttonShowControlPanel);
            this.Controls.Add(this.labelAsioDriverNames);
            this.Controls.Add(this.comboBoxAsioDriverNames);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormAsioControlPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ASIO Control Panel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAsioControlPanel_FormClosing);
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxAsioDriverNames;
        private System.Windows.Forms.Label labelAsioDriverNames;
        private System.Windows.Forms.Button buttonShowControlPanel;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hideToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.CheckBox checkBoxStartsWithWindows;
    }
}

